﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar2.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Descricao : ContentPage
	{
        private const float Vidro_Eletricos = 545; 
        private const float Travas_Eletricas = 260; 
        private const float Ar_Condicionado = 480; 
        private const float Camera_de_Re = 180; 
        private const float Cambio_G = 460; 
        private const float Suspensao_G = 380; 
        private const float Freios = 245;

        public string Vidros
        {
            get
            {
                return string.Format("Vidros Elétricos - R$ {0}", Vidro_Eletricos);
            }
        }

        public string Travas
        {
            get
            {
                return string.Format("Travas Elétricas - R$ {0}", Travas_Eletricas);
            }
        }

        public string ArCondicionado
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {0}", Ar_Condicionado);
            }
        }

        public string Camera
        {
            get
            {
                return string.Format("Câmera de Ré - R$ {0}", Camera_de_Re);
            }
        }

        public string Cambio
        {
            get
            {
                return string.Format("Câmbio - R$ {0}", Cambio_G );
            }
        }

        public string Suspensao
        {
            get
            {
                return string.Format("Suspensão - R$ {0}", Suspensao_G);
            }
        }

        public string Freio
        {
            get
            {
                return string.Format("Freios - R$ {0}", Freios);
            }
        }
        public string ValorTotal
        {
            get
            {
                return string.Format("Valor total: R$ {0}", Veiculo.Preco_Modelo + (IncluiVidros ? Vidro_Eletricos : 0) + (IncluiTravas ? Travas_Eletricas : 0) 
                    + (IncluiAr ? Ar_Condicionado : 0) + (IncluiCamera ? Camera_de_Re : 0) + (IncluiCambio ? Cambio_G : 0) 
                    + (IncluiSuspensao ? Suspensao_G : 0) + (IncluiFreios ? Freios : 0));

            }

        }
        bool incluiVidros;
        public bool IncluiVidros
        {
            get
            {
                return incluiVidros;
            }
            set
            {
                incluiVidros = value;
                OnPropertyChanged(nameof(ValorTotal));

            }
            
        }

        bool incluiTravas;
        public bool IncluiTravas
        {
            get
            {
                return incluiTravas;
            }
            set
            {
                incluiTravas = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiAr;
        public bool IncluiAr
        {
            get
            {
                return incluiAr;
            }
            set
            {
                incluiAr = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiCamera;
        public bool IncluiCamera
        {
            get
            {
                return incluiCamera;
            }
            set
            {
                incluiCamera = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiCambio;
        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiSuspensao;
        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }

        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));

            }

        }


        public Veiculo Veiculo { get; set; }
        public Descricao (Veiculo veiculo)
		{
			InitializeComponent ();

            this.Title = veiculo.Nome_do_Modelo;
            this.Veiculo = veiculo;
            this.BindingContext = this;
        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(Veiculo));
        }
    }
}