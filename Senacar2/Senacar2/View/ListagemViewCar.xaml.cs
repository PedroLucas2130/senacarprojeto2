﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacar2.View
{
    
    public class Veiculo
    {
        public string Nome_do_Modelo { get; set; }
        public float Preco_Modelo { get; set; }

        public string Preco_Formatado
        {
            get { return string.Format("R$ {0}", Preco_Modelo); }
        }

        public string Nome_da_Marca { get; set; }

    }
    public partial class ListagemViewCar : ContentPage
    { 

        public List<Veiculo> Veiculos { get; set; }
        

    
        public ListagemViewCar()
        {
            InitializeComponent();

            this.Veiculos = new List<Veiculo>
            {
                new Veiculo { Nome_da_Marca = "Land Rover", Nome_do_Modelo = "Evoque", Preco_Modelo = 200000},
                new Veiculo { Nome_da_Marca = "Ferrari", Nome_do_Modelo = "F12", Preco_Modelo = 200000 },
                new Veiculo { Nome_da_Marca = "Xiaomi", Nome_do_Modelo = "Bestune T77", Preco_Modelo = 120000  },
                new Veiculo { Nome_da_Marca = "Ford", Nome_do_Modelo = "Mustang", Preco_Modelo = 250000 },
                new Veiculo { Nome_da_Marca = "Volvo", Nome_do_Modelo = "XC90", Preco_Modelo = 7500000}
            };
            this.BindingContext = this;

            

            
        }

        private void ListViewSenacar_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var veiculo = (Veiculo)e.Item;
            DisplayAlert("Servico", string.Format("Você selecionou o carro {0}, valor: {1}", veiculo.Nome_do_Modelo, veiculo.Preco_Formatado), "Ok");
            Navigation.PushAsync( new Descricao(veiculo));
        }
    }
}
